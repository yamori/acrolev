// This module holds all functions for parsing CSVs, validation, and transformations to/from JSON

function occurrences(string1, substring) {
    var occurs = 0;
    for (var i = 0; i < string1.length; i++) {
        var c = string1.charAt(i);        
        if (c===substring) {occurs++;}
    }
    return occurs;
}

function validateAndParseCSVData(csv_data) {
    // This funciton will return the below object.
    //  If a validation error, the two error_* fields will be populated, with no json_object
    //  If no validation error, then error_* is empty, and json_obj returned.
    var returnObj = {error_msg: '', error_line: '', json_object: ''};
    var json_object_array = []; // for collecting the acronym pairs

    var csv_data_tokens = csv_data.split("\n");
    for (var n=0; n<csv_data_tokens.length; n++) {
        var current_line_token = csv_data_tokens[n].trim();

        // Skip if empty
        if (current_line_token.length==0) {continue;}
        
        // Check for a comma
        if ( occurrences(current_line_token,",")<1 ) {
            returnObj.error_msg = "This line needs a comma";
            returnObj.error_line = current_line_token;
            return returnObj;
        }

        // Parse for acronym and text
        var line_tokens = current_line_token.split(',');
        var acronym = line_tokens[0].trim();
        var text = line_tokens.slice(1).join(',').trim();

        // Check for the acronym, and text
        if (acronym.length==0) {
            returnObj.error_msg = "This line needs an acronym";
            returnObj.error_line = current_line_token;
            return returnObj;
        }
        if (text.length==0) {
            returnObj.error_msg = "This line needs an acronym definition";
            returnObj.error_line = current_line_token;
            return returnObj;
        }

        // Create the JSON object
        json_object_array.push( {acronym: acronym, text: text} );
    }

    // This code and 'return' clause only reached if zero invalidations
    json_object_array.sort(function(a, b){ 
        // return b.acronym - a.acronym;
        var nameA=a.acronym.toLowerCase(), nameB=b.acronym.toLowerCase();
        if (nameA < nameB) //sort string ascending
            return -1;
        if (nameA > nameB)
            return 1;
        return 0; 
    });
    returnObj.json_object = json_object_array;
    return returnObj;
}

function jsonObjToCSV(jsonObject) {
    // turns this struct into csv for edit page, 
    // [{"acronym":"abc","text":"alpha bravo c"},{"acronym":"xyz","text":"xylaphone yank z"}]
    var resultantString = "";

    for (var n=0; n<jsonObject.length; n++) {
        jsonObject_item = jsonObject[n];
        resultantString += (jsonObject_item.acronym + "," + jsonObject_item.text + "\n");
    }

    return resultantString;
}

module.exports = {
    validateAndParseCSVData,
    jsonObjToCSV
}