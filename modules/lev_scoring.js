const LEVENSHTEIN = require('js-levenshtein');

function modifiedLevenshtein(query, foreign) {
  var query_UC = query.toUpperCase();
  var foreign_UC = foreign.toUpperCase();
  var lev_value = LEVENSHTEIN(query_UC,foreign_UC);
  if (lev_value==0) { return lev_value; }
  if (lev_value>0) { lev_value+=10; }
  if (foreign_UC.length>=1 && query_UC.length>=1 && foreign_UC[0]==query_UC[0]) { lev_value-=5; }
  if (foreign_UC.length>=2 && query_UC.length>=2 && foreign_UC[1]==query_UC[1]) { lev_value-=3; }
  return lev_value;
}

function levScoring(acronyms_json, queryString) {

  var thresholdScore = -1;
  var topLevd = []; // empty to start
  var desiredNumber = 5;

  var deepClone = JSON.parse(JSON.stringify(acronyms_json));

  for (index = 0; index < deepClone.length; index++) {
    // Comparisons done in UpperCase
    var acronym = deepClone[index].acronym.toUpperCase();
    var queryString = queryString.toUpperCase().trim();
    var modified_lev_value = modifiedLevenshtein(acronym, queryString);
    // modifiedLevenshtein is augmented/inverted into 0-10 scale for intuitivity (word?)
    var inverted_lev = Math.max(0, 10-modified_lev_value);
    deepClone[index].lev_value = inverted_lev;

    var newObj = {"acronym": deepClone[index].acronym, "text": deepClone[index].text, "lev_value": inverted_lev};

    // Attachment algorithm
    if (topLevd.length < desiredNumber) {
      topLevd.push(newObj);
    } else if (inverted_lev >= thresholdScore) {
      // console.log(topLevd);
      topLevd.pop();
      // console.log(topLevd);
      topLevd.push(newObj);
      topLevd.sort(function(a,b) { return b.lev_value - a.lev_value; });
      thresholdScore = topLevd[topLevd.length-1].lev_value;
    }
  }
  return topLevd;
}


module.exports = {
  modifiedLevenshtein,
  levScoring
}
