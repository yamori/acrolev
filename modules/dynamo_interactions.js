
// Load the AWS SDK for JS
var AWS = require("aws-sdk");

// Set a region to interact with (make sure it's the same as the region of your table)
AWS.config.update({region: 'us-east-1'});

// Set a table name that we can use later on
const tableName = "acroLev_dev";

// Create the Service interface for DynamoDB
var dynamodb = new AWS.DynamoDB({apiVersion: '2012-08-10'});

// Create the Document Client interface for DynamoDB
var ddbDocumentClient = new AWS.DynamoDB.DocumentClient();

// This is case-insensitive, downcases before querying
async function queryForSpace(space_query){
    try {
        var params = {
            Key: {
             "space": {"S": space_query.toLowerCase() } 
            },
            TableName: tableName
        };
        var result = await dynamodb.getItem(params).promise();
        // Two element array: [ jsonAcroyms, displayNameForSpace, secret_hash ]
        return [ JSON.parse(result["Item"]["acronym_json_obj"]["S"]), 
                  result["Item"]["space_displayStr"]["S"], 
                  result["Item"]["secret_hash"]["S"],
                  result["Item"]["space"]["S"]
                ];
    } catch (error) {
        console.error(error);
        return {};
    }
}

// This is case-insensitive, downcases before querying
async function queryForSpaceBoolean(space_query) {
  // Returns T/F if space exists
    try {
        var params = {
            Key: {
             "space": {"S": space_query.toLowerCase() }
            },
            TableName: tableName
        };
        var result = await dynamodb.getItem(params).promise();
        if ( Object.keys(result).length === 0 && result.constructor === Object ) {
          return false;
        } else {
          return true;
        }
    } catch (error) {
        console.error(error);
        return false;
    }
}

async function putItem(jsonObj) {
  // Assumes it has items: space, space_displayStr, secret_hash
  jsonObj.TableName = tableName;  // Add table name, required.
  try {
      var result = await dynamodb.putItem(jsonObj).promise();
      return true;
  } catch (error) {
      console.error(error);
      return false;
  }
}

async function updateItem(space_name,json_object) {
  var params = {
    TableName: tableName,
    Key:{
    "space": space_name
    },
    UpdateExpression: "set acronym_json_obj=:p",
    ExpressionAttributeValues:{
    ":p": json_object // Expecting this to already be stringified
    },
    ReturnValues:"UPDATED_NEW"
  };

  ddbDocumentClient.update(params, function(err, data) {
    if (err) {
      console.log(err);
      return false;
    } else {
      return true;
    }
  });
  return true;
}

module.exports = {
  queryForSpace,
  queryForSpaceBoolean,
  putItem,
  updateItem
}
