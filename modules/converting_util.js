// Use this module to convert csv's into JSONs., meant for DEV purpsoes
//   (see bottom of file for sample use)

function readFile(fileName) {
  // Returns array split by \n
  var fs = require("fs");
  var textByLineArray = fs.readFileSync(fileName).toString().split("\n");
  return textByLineArray;
}

function convertArrayToJSON(textByLineArray) {
  var targetJSONObject = [];
  for (var i = 0; i < textByLineArray.length; i++) {
    if (textByLineArray[i].trim().length==0) { continue; }
    var tokens = textByLineArray[i].split(",");
    targetJSONObject.push( {"acronym": tokens.shift(), "text": tokens.join()} );
  }
  // Use this if you want to beautify the JSON, but that's a lot of carriage returns.
  // var jSONStringPrettyJSON.stringify(targetJSONObject, null, 2);
  var jSONString = JSON.stringify(targetJSONObject);
  return jSONString;
}

function writeToFile(outfile, string) {
  fs = require('fs');
  fs.writeFileSync(outfile, string);
}

// Sample use
// var textByLineArray = readFile("demo_mlb.csv");
// var jSONString = convertArrayToJSON(textByLineArray);
// writeToFile("demo_mlb.json",jSONString);
