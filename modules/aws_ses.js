
// Load the AWS SDK for JS
var AWS = require("aws-sdk");
AWS.config.update({region: 'us-east-1'});
var awsSES = new AWS.SES({apiVersion: '2010-12-01'});

function createParamsObject(toEmail, spaceName, secretLink) {
    var params = {
        Destination: { 
        CcAddresses: [],
        ToAddresses: [ toEmail ]
        },
        Message: { 
        Body: { 
            Text: {
            Charset: "UTF-8",
            Data: "Don't share this link!\nUse this to add, edit, and publish your acronyms:\n\n" + secretLink
            }
        },
        Subject: {
            Charset: 'UTF-8',
            Data: 'Acronym Ninja link: ' + spaceName
        }
        },
        Source: 'no-reply@' + APP_EMAIL_DOMAIN,
        ReplyToAddresses: [],
    };
    return params;
}

function constructSecretLink(protocol, host, spaceName, secretHash) {
    // 'host' doesn't work here when CLoudFront+API-GW
    // return encodeURI( protocol + "://" + host + "/edit_space?space=" + spaceName + "&secret_hash=" + secretHash );
    return encodeURI( protocol + "://" + APP_DOMAIN + "/edit_space?space=" + spaceName + "&secret_hash=" + secretHash );
}

async function sendEmailWithLink(toEmail, spaceName, secretLink) {
    var paramsObject = createParamsObject(toEmail, spaceName, secretLink);
    try {
        var result = await awsSES.sendEmail(paramsObject).promise();
        console.log(JSON.stringify(result));
    } catch (error) {
        console.error(error);
    }
}

module.exports = {
    sendEmailWithLink,
    constructSecretLink
  }