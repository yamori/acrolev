const PAGE_NAME = 'Acronym Ninja';
global.APP_DOMAIN = "www.acronym.ninja";
global.APP_EMAIL_DOMAIN = "acronym.ninja";

var express = require('express');
var router = express.Router();
const bodyParser = require('body-parser');

var lev_scoring = require('../modules/lev_scoring');
var dynamo_interactions = require('../modules/dynamo_interactions');
var hashing = require('../modules/md5');
var csv_utility = require('../modules/csv_utility');
var aws_ses = require('../modules/aws_ses');

// Landing page
router.get('/', function(req, res, next) {
  if (req.query.space_query===undefined) {
    // First entrance of a user
    res.render('index', { site_title:PAGE_NAME });
  } else {
    var space_query_string = req.query.space_query.trim();
    dynamo_interactions.queryForSpaceBoolean(space_query_string).then(spaceBoolean => {
      if (spaceBoolean) {
        res.redirect('/n?space_query=' + space_query_string);
      } else {
        req.flash('warning', space_query_string);
        res.locals.message = req.flash();
        res.render('index', { site_title:PAGE_NAME }); // this EJS page will clear the URL query params
      }
    });
  }
});

// Page specific to acronym space
router.get('/n', function(req, res, next) {
  dynamo_interactions.queryForSpace(req.query.space_query).then(returnObj => {
    if ( Object.keys(returnObj).length === 0 && returnObj.constructor === Object ) {
      req.flash('warning', req.query.space_query);
      res.locals.message = req.flash();
      res.render('index', { site_title:PAGE_NAME }); // this EJS page will clear the URL query params
    } else {
      res.render('space', {site_title:PAGE_NAME, APP_DOMAIN:APP_DOMAIN, ACRONYMS_JSON: returnObj[0], space: returnObj[1]});
    }
  });
});

// Recieves ajax requests
router.post('/queryAcronymAjax', function(req, res) {
  var userInput = req.body.userInput;
  dynamo_interactions.queryForSpace(req.body.space).then(spaceJsonObject => {
    var returnObjects = lev_scoring.levScoring(spaceJsonObject[0], userInput);
    res.render('partials/resultHTML', {returnObjects: returnObjects});
  });
});

// Page/form for new space creation
router.get('/new_space', function(req, res) {
  res.render('new_space', {site_title:PAGE_NAME} );
});

// Page/form for new space creation
router.post('/new_space', function(req, res) {
  if ( !/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(req.body.email) ) {
    req.flash('warning', "The email doesn't appear to be valid...");
    res.locals.message = req.flash();
    res.render('new_space', {site_title:PAGE_NAME, email: req.body.email, space_name: req.body.space_name} );
    return;
  } 
  var requestedSpaceName = req.body.space_name.trim();
  if (requestedSpaceName.length<3) {
    req.flash('warning', "Please use at least 3 characters for the space name...");
    res.locals.message = req.flash();
    res.render('new_space', {site_title:PAGE_NAME, email: req.body.email, space_name: req.body.space_name} );
    return;
  }

  dynamo_interactions.queryForSpaceBoolean(requestedSpaceName).then(spaceBoolean => {
    if (spaceBoolean) {
      req.flash('warning', "Sorry, looks like that space is already taken...");
      res.locals.message = req.flash();
      res.render('new_space', {site_title:PAGE_NAME, email: req.body.email, space_name: requestedSpaceName} );
      return;
    } else {
      // Good for a new space
      var secretString = req.body.email+Date.now()+Math.floor(Math.random() * 100).toString();
      var secretHash = hashing.MD5(secretString);
      var jsonObj = {
        Item: {
          "space": {
            "S": requestedSpaceName.toLowerCase()
          },
          "space_displayStr": {
            "S": requestedSpaceName
          },
          "owner_email": {
            "S": req.body.email.trim()
          },
          "secret_hash": {
            "S": secretHash
          },
          "acronym_json_obj": {
            "S": "[]" // Intentionally start this as zero length
          }
        }
      }
      // TechDebt !, change these two promises into chains, chain them, serialize, or whatever correct way
      dynamo_interactions.putItem(jsonObj).then(result => {
        if (result) {
          var secretLink = aws_ses.constructSecretLink(req.protocol, req.headers.host, requestedSpaceName.toLowerCase(), secretHash);
          aws_ses.sendEmailWithLink(req.body.email, requestedSpaceName, secretLink).then(result => {
            req.flash('success', "Check your email!  Link (to edit and publish) is on the way...");
            res.locals.message = req.flash();
            res.render('index', { site_title:PAGE_NAME });
          }).catch(result => {
            console.error(result);
          });
        } else {
          req.flash('warning', "Hmm, something went wrong...");
          res.locals.message = req.flash();
          res.render('new_space', {site_title:PAGE_NAME, email: req.body.email, space_name: req.body.space_name} );
        }
      });
    }
  });
});

// Page/form for editing a space, must include the correct hash to get through
router.get('/edit_space', function(req, res) {
  dynamo_interactions.queryForSpace(req.query.space).then(spaceJsonObject => {
    if (spaceJsonObject[2] === req.query.secret_hash) {
      var existingAcronymsAsCSV = csv_utility.jsonObjToCSV(spaceJsonObject[0]);
      var space_link = encodeURI( req.protocol+"://" + APP_DOMAIN + "/n?space_query=" + req.query.space );
      // Because behind API GW, this isn't working
      // var space_link = encodeURI( req.protocol+"://"+req.headers.host + "/n?space_query=" + req.query.space );
      res.render('edit_space', { site_title: PAGE_NAME, 
        space_name: req.query.space, 
        secret_hash: req.query.secret_hash,
        textfield_csv: existingAcronymsAsCSV,
        space_link: space_link }
      );
    } else {
      req.flash('warning', req.query.space);
      res.locals.message = req.flash();
      res.render('index', { site_title: PAGE_NAME });
    }
  });
});

// Page/form for new space creation
router.post('/edit_space', function(req, res) {
  var returnObj = csv_utility.validateAndParseCSVData(req.body.csv_data);

  if (returnObj.json_object.length>0) {
    // First do a query, to check the hash
    var space_name = req.body.space_name.trim().toLowerCase();

    dynamo_interactions.queryForSpace(space_name).then(spaceJsonObject => {
      //spaceJsonObject[3] is already downcased
      if (spaceJsonObject[3]===space_name && spaceJsonObject[2]===req.body.secret_hash) { 
        // Space and hash matches, proceed
        var json_obj_String = JSON.stringify(returnObj.json_object);
        dynamo_interactions.updateItem(space_name.toLowerCase(),json_obj_String).then(spaceJsonObject => {
          res.send( {error_msg: '', error_line: '', success_msg: "Success"});
        });
      } else {
        res.send( {error_msg: "Hmmm, something went wrong", error_line: '', success_msg: ''});
      }
    });
  } else {
    res.send( {error_msg: returnObj.error_msg, error_line: returnObj.error_line, success_msg: ''});
  }
  // Client side JS expects return of {error_msg: ?, error_line: ?, success_msg: ?}
});

module.exports = router;
