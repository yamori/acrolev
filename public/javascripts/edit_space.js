//    button click
function submitCSVContentSpaceEdit() {
    var csvData = $("#csvFormInputSpace").val().trim();
    if (csvData.length==0) {
      $('#csvFormInputSpace').focus();
      return; // Prevent zero query
    } 

    // UI stuff
    $("#csvFormInputSpace").attr("disabled", "disabled");
    $('#editSpaceSubmit').attr('disabled',true);

    $.ajax({
      url: "/edit_space",
      type: "POST",
      data: {csv_data: csvData, space_name: space_name, secret_hash: secret_hash},
      dataType: "text",
          success: function(response, status, http) {
            $("#csvFormInputSpace").removeAttr("disabled");
            $('#editSpaceSubmit').attr('disabled',false);
            var returnJSON = JSON.parse(response);
            if (returnJSON.error_msg.length>0) {
                $("#csvEditSuccessDiv").hide();
                $("#csvEditWarningDiv").show();
                $("#csvEditWarningDiv_msg").html(returnJSON.error_msg);
                $("#csvEditWarningDiv_line").html(returnJSON.error_line);
            } else if (returnJSON.success_msg.length>0) {
                $("#csvEditWarningDiv").hide();
                $("#csvEditSuccessDiv").show();
                $("#csvEditSuccessDiv_msg").html(returnJSON.success_msg);
            }
          },
          error: function(xhr, status, error) {
            console.log(error);
          }
      });
  }
  
  $(document).ready(function() {
  
    $("#editSpaceSubmit").button().click(function(e){
        e.preventDefault();
        submitCSVContentSpaceEdit();
    });
  });
  