//    button click
function queryAcronymAjax() {
  var queryData = $("#mainSearchText").val().trim();
  if (queryData.length==0) {
    $('#mainSearchText').focus();
    return;
  } // Prevent zero query

  // UI stuff
  $("#mainSearchText").attr("disabled", "disabled");
  $('#mainSubmit').attr('disabled',true);

  $.ajax({
    url: "/queryAcronymAjax",
    type: "POST",
    data: {userInput: queryData, space: space},
    dataType: "text",
        success: function(response, status, http) {
            if (response) {
              $("#magnifying-glass").hide();
              $("#mainSearchText").removeAttr("disabled");
              $('#mainSubmit').attr('disabled',false);
              $("#tableBodyContent").html(response);
            }
        }
    });
}

$(document).ready(function() {

  $("#mainSubmit").button().click(function(){
      queryAcronymAjax();
  });

  $('#mainSearchText').keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      queryAcronymAjax();
      return false;
    }
  });

  var anchor_hash = window.location.hash.substr(1);

  // Set focus
  var fieldInput = $('#mainSearchText');
  var fldLength= fieldInput.val().length;
  if (anchor_hash.length ==0) {
    // Only focus on the input field if no hash in url
    fieldInput.focus();
    fieldInput[0].setSelectionRange(fldLength, fldLength);
  }
});
