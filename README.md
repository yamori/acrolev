# AcroLev

WebApp to serve up acronym publishing in an easy way.  For enterprise/anyone.

## The Tech

### Requirements

```
acrolev $ npm -v
6.4.1

acrolev $ node -v
v12.16.3
```

And AWS credentials available to the SDK used in the code, either through an ec2 role or locally `~/.aws/credentials`.

- SES:SendEmail
- dynamoDB:PutItem
- dynamoDB:GetItem
- dynamoDB:UpdateItem

### Stand Up
```
// primary intent isn't to test DynamoDB instance, but ends up hitting it during test
npm test 

npm start
```

Note: `npm start` invokes `app.js`, but any Serverless deployment (described in `serverless.yml`) will invoke `serverless_app.js`.  Therefore these two js should be kept in sync.

## Serverless Deployment Process

1) Update static assets (currently manual process) in the S3 bucket: `https://acro-lev-static-assets.s3.amazonaws.com/prod/`

2) ```serverless deploy```

3) Udpate the Lambda to use the `AcroLev_Lambda_dev` role.

4) Cloudfront: change the origin to point the the generated API Gateway's origin and path.

## EC2 Instance Build-Out

How to setup from a fresh Amazon Linux2 AMI

```
ssh-keygen
sudo yum update -y
sudo yum install git -y

curl -o https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash
. ~/.nvm/nvm.sh
nvm install node

cat .ssh/id_rsa.pub
// bring it into the bitbucket repo
ssh -T git@bitbucket.org // adds to known host
git clone (gihub repo)/acrolev.git
cd acrolev/

// set NODE_ENV is set to production; export blah
npm install // This will respect the above env var, not do devDependenies
npm test
nohup npm start &

// ... then kill
ps -ef | grep npm
kill (ID from above)
```
