//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();


chai.use(chaiHttp);
//Our parent block
describe('HTML content', () => {
/*
  * Test the /GET route
  */
  describe('Site Title', () => {
      it('it should appear at GET / (root)', (done) => {
        chai.request(server)
            .get('/')
            .end((err, res) => {
                  res.should.have.status(200);
                  chai.expect(res.text).to.contain("Acronym Ninja");
              done();
            });
      });
      it('it should appear on a space page /n?space_query=demo_mlb', (done) => {
        chai.request(server)
            .get('/n?space_query=demo_mlb')
            .end((err, res) => {
                  res.should.have.status(200);
                  chai.expect(res.text).to.contain("Acronym Ninja");
              done();
            });
      });
  });

});
