var assert = require('assert');
var csv_utility = require('../modules/csv_utility');

describe('CSV_Utility module', function() {
  
    describe('#validateAndParseCSVData()', function() {
        
        var sampleInvalidCSVInput = "fdfdf\n,alph brave charlie\nABC,";

        it('should return no json_obj if invalid input', function() {
            var returnObj = csv_utility.validateAndParseCSVData(sampleInvalidCSVInput);
            assert.equal(returnObj.json_object, '');
        });

        it('should detect if comma missing', function() {
            var returnObj = csv_utility.validateAndParseCSVData(sampleInvalidCSVInput);
            assert.equal(returnObj.error_msg, "This line needs a comma");
            assert.equal(returnObj.error_line, sampleInvalidCSVInput.split("\n")[0]);
        });

        it('should detect if acronym is missing', function() {
            var returnObj = csv_utility.validateAndParseCSVData(sampleInvalidCSVInput.split("\n").slice(1).join("\n"));
            assert.equal(returnObj.error_msg, "This line needs an acronym");
            assert.equal(returnObj.error_line, sampleInvalidCSVInput.split("\n")[1]);
        });

        it('should detect if text is missing', function() {
            var returnObj = csv_utility.validateAndParseCSVData(sampleInvalidCSVInput.split("\n")[2]);
            assert.equal(returnObj.error_msg, "This line needs an acronym definition");
            assert.equal(returnObj.error_line, sampleInvalidCSVInput.split("\n")[2]);
        });

        // JSON object

        var sampleValidCSVInput = "ABC, fjf \nXYZ ,  acronym doesn't need to match up, text/def can include comma";
        var arconym1 = "ABC"; 
        var text1 = "fjf"; // should strip white space
        var arconym2 = "XYZ";
        var text2 = "acronym doesn't need to match up, text/def can include comma"; // should accomodate commas

        it('should not have error messages when input is valid', function() {
            var returnObj = csv_utility.validateAndParseCSVData(sampleValidCSVInput);
            assert.equal(returnObj.error_msg, '');
            assert.equal(returnObj.error_line, '');
        });
        it('should return a json object with correct values', function() {
            var returnObj = csv_utility.validateAndParseCSVData(sampleValidCSVInput);
            assert.equal(returnObj.json_object[0].acronym, arconym1);
            assert.equal(returnObj.json_object[0].text, text1);
            assert.equal(returnObj.json_object[1].acronym, arconym2);
            assert.equal(returnObj.json_object[1].text, text2);
        });
        it('should alphabetize', function() {
            var returnObj = csv_utility.validateAndParseCSVData("XYZ,xylayz\nGHI,golfHI\nABC,Alpha bravo ");
            assert.equal(returnObj.json_object[0].acronym, "ABC");
            assert.equal(returnObj.json_object[1].acronym, "GHI");
            assert.equal(returnObj.json_object[2].acronym, "XYZ");
        });
        it('should skip over empty lines', function() {
            var returnObj = csv_utility.validateAndParseCSVData("ABC, fjf \n\nXYZ ,  acronym doesn't need to match up, text/def can include comma");
            assert.equal(returnObj.json_object.length, 2);
            assert.equal(returnObj.json_object[0].acronym, arconym1);
            assert.equal(returnObj.json_object[0].text, text1);
            assert.equal(returnObj.json_object[1].acronym, arconym2);
            assert.equal(returnObj.json_object[1].text, text2);
        });
    });

    describe('#jsonObjToCSV()', function() {

        it('should produce converted CSV content', function() {
            var jsonObj = [
                {"acronym": "ABC", "text": "alpha brav charles"},
                {"acronym": "GFH", "text": "golf foxtrot hotelier"}
            ];
            var expectedCSV = "ABC,alpha brav charles\nGFH,golf foxtrot hotelier\n";

            var returnString = csv_utility.jsonObjToCSV(jsonObj);
            assert.equal(expectedCSV, returnString);
        });
    });
  
  });
  