const LEVENSHTEIN = require('js-levenshtein');

var assert = require('assert');
var lev_scoring = require('../modules/lev_scoring');

// Import sample acrony JSON
const SAMPLE_ACRONYMS = './sample_data/demo_nasa.json';
const fs = require('fs');
let rawdata = fs.readFileSync(SAMPLE_ACRONYMS);
const ACRONYMS_JSON = JSON.parse(rawdata);

describe('Lev_Scoring module', function() {
  
  describe('#modifiedLevenshtein()', function() {
    it('should score a lev of zero', function() {
      var score = lev_scoring.modifiedLevenshtein("xyz", "xyz");
      assert.equal(score, 0);
    });
    it('should be lev+3 for complete mismatch', function() {
      var score = lev_scoring.modifiedLevenshtein("xyz", "abc");
      assert.equal(score, 3+10);
    });
    it('should be lev+3-5 for complete mismatch except first letter', function() {
      var score = lev_scoring.modifiedLevenshtein("xyz", "Xbc");
      assert.equal(score, 2+10-5);
    });
    it('should be lev+3-3 for complete mismatch except second letter', function() {
      var score = lev_scoring.modifiedLevenshtein("xyz", "ayc");
      assert.equal(score, 2+10-3);
    });
    it('should be lev+3-5-3 for complete mismatch except first and second letter', function() {
      var string1 = "xyzaaa";
      var string2 = "xycaaa";
      var score = lev_scoring.modifiedLevenshtein(string1, string2);
      assert.equal(score, LEVENSHTEIN(string1,string2)+10-5-3);
    });
  });


  describe('#levScoring()', function() {

    // This is all specific to the JSON file at SAMPLE_ACRONYMS (referenced above)
    var trialAcronymMatch = "ASAG";
    it('should return four(4) results', function() {
      var returnObj = lev_scoring.levScoring(ACRONYMS_JSON, trialAcronymMatch);
      assert.equal(returnObj.length, 5);
    });
    it('should be case insensitive', function() {
      var returnObj = lev_scoring.levScoring(ACRONYMS_JSON, trialAcronymMatch.toLowerCase());
      assert.equal(returnObj[0].acronym, trialAcronymMatch);
      assert.equal(returnObj[0].lev_value, 10);  // Max value, 10, indicates match
    });
    it('should strip white space from query, results accordingly', function() {
      var returnObj = lev_scoring.levScoring(ACRONYMS_JSON, 'g');
      assert.equal(returnObj[0].acronym, "GAO");
      assert.equal(returnObj[1].acronym, "GEO");
      var returnObj2 = lev_scoring.levScoring(ACRONYMS_JSON, ' g '); //Dear god, superfluous white space!
      assert.equal(returnObj2[0].acronym, "GAO");
      assert.equal(returnObj2[1].acronym, "GEO");
    });
    it('should return correct acronym when called', function() {
      var returnObj = lev_scoring.levScoring(ACRONYMS_JSON, trialAcronymMatch);
      assert.equal(returnObj[0].acronym, trialAcronymMatch);
      assert.equal(returnObj[0].lev_value, 10);  // 10 indicates a match
    });
    it('should have a score floor of zero(0)', function() {
      var bad_json = [{acronym:"xxxxxxxxxxx", text: "t"}]; 
      var returnObj = lev_scoring.levScoring(ACRONYMS_JSON, "yyyyyyyyyyy");
      assert.equal(returnObj[0].lev_value, 0);  // 0 is the floor
    });

  });
});
